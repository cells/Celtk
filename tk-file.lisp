;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cells; -*-
#|

    Celtk -- Cells, Tcl, and Tk

Copyright (C) 2006, 2007 by Kenneth Tilton
This file: Copyright (C) 2007 by Frank Goenninger

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#

(in-package :Celtk)

(defun do-tk-get-open-file (self args)
  (tk-open-file self args))

(defmacro tk-get-open-file (self args)
  `(tk-format-now "Tk_getOpenFile"))
