;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cells; -*-
#|

    Celtk -- Cells, Tcl, and Tk

Copyright (C) 2006 by Kenneth Tilton

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#

;;; $Header$

(in-package :Celtk)

;;; --- running a Celtk (window class, actually) ------------------------------

(eval-now!
 (export '(tk-scaling

           run-window-using-context
           mk-run-window-context
           
           run-window
           test-window

           *ctk-dbg*

           defcommand
           )))

(defparameter *ctk-dbg* nil)

;;; --- commands --------------------------------------------------------------

(defmacro defcommand (name)
  (let ((do-on-name (read-from-string (format nil "DO-ON-~a" name)))
        (^on-name (read-from-string (format nil "^ON-~a" name))))

    `(progn

       (defmethod ,do-on-name (self &rest args)
         (bwhen (cmd (,^on-name))
           (apply cmd self args))
         0)

       (defcallback ,do-on-name :int
           ((client-data :pointer)(interp :pointer)(argc :int)(argv :pointer))
         (declare (ignore client-data))
         (let ((*tki* interp)
               (args (loop for argn upfrom 1 below argc
                        collecting (mem-aref argv :string argn))))
           (bif (self (gethash (car args) (dictionary *tkw*)))
                (progn
                  (trc "defcommand > " ',^on-name self (cdr args))
                  (apply ',do-on-name self (rest args)))
                (progn
                  (break ",do-on-name> Target widget ~a does not exist" (car args))
                  #+anyvalue? (tcl-set-result interp
                                              (format nil ",do-on-name> Target widget ~a does not exist" (car args))
                                              (null-pointer))
                  1)))))))

(defcommand command)
;
; see notes elsewhere for why Tcl API deficiencies require augmented key handling via app virtual events
;
(defcommand key-down)
(defcommand key-up)
(defcommand double-click-1)
(defcommand double-click-2)
(defcommand double-click-3)

;;; --- running a Celtk (window class, actually) -----------------------------

(defmd run-window-context ()
  root-class
  resetp
  window-initargs
  tk-packages-to-load

  ;; Default initargs

  :resetp t
 
  ;; Specify here the Tcl/Tk packages to load after Tcl/Tk init.
  ;; Format is: list of  (package-name init-function) pairs.
  :tk-packages-to-load (list
                         '("snack" nil)
                         '("tile"  (lambda ()
                                     (ctk:tk-format-now "namespace import -force ttk::*")))
                         '("QuickTimeTcl" nil)
                         '("snack" (lambda ()
                                     (ctk:tk-format-now "snack::sound s")))))

(defmacro mk-run-window-context (root-class &rest args)
  `(make-instance 'run-window-context :root-class ,root-class ,@args))
  
(defparameter *rwc* nil "This is the single instance of run-window-context. Holds call parameters for run-window. Needed because run-window needs to be a function with no arguments on Lispworks.")

(defun %do-run-window ()
  "Lowest level call to %run-window - implementation and platform specific 
   stuff should go into this function."
  
  ;;(%run-window) ;; frgo, 2007-09-28:
  ;;                 DEBUG - call %run-window directly even on LW
  
  #+lispworks
  (let* ((bindings (cons '(*tkw* . *tkw*) mp:*process-initial-bindings*)) ;; UGLY ...
         (bindings (cons '(*tki* . *tki*) bindings))                      ;; there has to be a
         (bindings (cons '(*app* . *app*) bindings))                      ;; better way ...
         (bindings (cons '(*rwc* . *rwc*) bindings))                      ;; frgo, 2007-09-26
         (mp:*process-initial-bindings* bindings))
    (%run-window))
  
  #-lispwoks (%run-window)
  )

(defun run-window (root-class &optional (resetp t) &rest window-initargs)
  (declare (ignorable root-class))
  
  ;; Save call parameters into *rwc* context
  (setq *rwc* (make-instance 'run-window-context
                             :root-class root-class
                             :resetp resetp
                             :window-initargs window-initargs))

  ;; Call internal run-window funtion
  (%do-run-window))

(defmethod run-window-using-context ((rwc run-window-context))
  (declare (ignorable root-class))
  
  ;; Save call into *rwc* context
  (let ((*rwc* rwc))
    
    ;; Call internal run-window funtion
    (%do-run-window)))

(defun tk-package-require (tk-package)
  (assert (stringp tk-package) () "Error: Parameter tk-package is not a string.")
  (tk-format-now "package require ~a" tk-package))

(defun %run-window ()
  "This function is intented to be called by 'run-window. It relies on the call parameters to be stored in *rwc*."
  
  (assert *rwc* () "Error: Global call context *rwc* for '%run-window is not initialized.")

  ;; Get call parameters from *rwc*
  (let ((root-class (root-class *rwc*))
        (resetp (resetp *rwc*))
        (window-initargs (window-initargs *rwc*))
        (tk-packages-to-load (tk-packages-to-load *rwc*)))

    ;; Ensure clean start situation
    
    (setf *tkw* nil)

    (when resetp
      (cells-reset 'tk-user-queue-handler))

    (tk-interp-init-ensure)

    ;; Initialize Tcl/Tk
    (setf *tki* (Tcl_CreateInterp))
    
    (tk-app-init *tki*)  ;; Inits Tk
    (tk-togl-init *tki*) ;; Inits the Tcl/Tk OpenGL Widget TOGL

    (trc "Tcl/Tk and Togl initialized." *tki*)

    ;; Load Tcl/Tk packages (as they are defined in *rwc*.tk-packages-to-load)

    (dolist (pkg-load-info tk-packages-to-load)
      (let ((tk-package (first pkg-load-info))
            (init-fn (second pkg-load-info)))
        (when tk-package
          (tk-package-require tk-package))
        (when (and init-fn (functionp init-fn))
          (trc "*** Calling Tcl/Tk package init function" init-fn)
          (funcall init-fn))))
    
    ;; Setup Tcl/Tk to be able to interact with Celtk
    (tk-format-now
       "proc TraceOP {n1 n2 op} {event generate $n1 <<trace>> -data $op}")

    (tcl-create-command *tki* "do-on-command"
                        (get-callback 'do-on-command)
                        (null-pointer) (null-pointer))

    ;; These next exist because of limitations in the Tcl API. eg, the
    ;; keypress event does not include enough info to extract the keysym
    ;; directly, and the function to extract the keysym is not exposed.
    ;; The keysym, btw, is the portable representation of key events.

    (tcl-create-command *tki* "do-key-down"
                        (get-callback 'do-on-key-down)
                        (null-pointer) (null-pointer))
      
    (tcl-create-command *tki* "do-key-up"
                        (get-callback 'do-on-key-up)
                        (null-pointer) (null-pointer))

    (tcl-create-command *tki* "do-double-click-1"
                        (get-callback 'do-on-double-click-1)
                        (null-pointer) (null-pointer))
    (tcl-create-command *tki* "do-double-click-2"
                        (get-callback 'do-on-double-click-2)
                        (null-pointer) (null-pointer))
    (tcl-create-command *tki* "do-double-click-3"
                        (get-callback 'do-on-double-click-3)
                        (null-pointer) (null-pointer))
        
    (trc ";;; Celtk: Tcl/Tk setup done. Now about to create window.")

    ;; Create the application window
    
    (with-integrity () ;; w/i somehow ensures tkwin slot gets populated
       (setf *app*
             (make-instance 'application
                            :kids (c? (the-kids
                                       (setf *tkw* (apply 'make-instance root-class
                                                          :fm-parent *parent*
                                                          window-initargs))))
                            )))
    
    (assert (tkwin *tkw*)) ;; really there ?

    (trc ";;; Celtk: Tcl/Tk window created.")

    ;; And ... show it!
    (tk-format `(:fini) "wm deiconify .")

    ;; Default key bindings
    
    #-its-alive! (tk-format-now "bind . <Escape> {destroy .}")
    ;;
    ;; See above for why we are converting key x-events to application
    ;; key virtual events:

    (tk-format-now "bind . <KeyPress> {do-key-down %W %K}")
    (tk-format-now "bind . <KeyRelease> {do-key-up %W %K}")
    
    (tk-format-now "bind . <Double-ButtonPress-1> {do-double-click-1 %W %K; break}")
    (tk-format-now "bind . <Double-ButtonPress-2> {do-double-click-2 %W %K; break}")
    (tk-format-now "bind . <Double-ButtonPress-3> {do-double-click-1 %W %K; break}")

    ;; Call the window class's init function prior to enter event loop
    (bwhen (ifn (start-up-fn *tkw*))
      (funcall ifn *tkw*))

    ;; Kenny Tilton specials on next 4 lines
    #+cg (cg:kill-spash-screen)
    (unless #-rms-s3 nil
            #+rms-s3 (b-when bail$ (clo::rms-get :login "announce" )
                       (not (eval (read-from-string bail$)))))
    
    ;; Finally enter event loop to process events
    (tcl-do-one-event-loop)))

(defun ensure-destruction (w key)
  (declare (ignorable key))
  ;(TRC "ensure.destruction entry" key W (type-of w))
  (unless (find w *windows-being-destroyed*)
    ;(TRC "ensure.destruction not-to-being" key W)
    
    (let ((*windows-being-destroyed* (cons w *windows-being-destroyed*)))
      (not-to-be w))))

(defparameter *keyboard-modifiers*
  (loop with km = (make-hash-table :test 'equalp)
      for (keysym mod) in '(("Shift_L" :shift)
                          ("Shift_R" :shift)
                          ("Alt_L" :alt)
                          ("Alt_R" :alt)
                          ("Control_L" :control)
                          ("Control_R" :control))
      do (setf (gethash keysym km) mod)
      finally (return km)))

(defun keysym-to-modifier (keysym)
  (gethash keysym *keyboard-modifiers*))

(defmethod widget-event-handle ((self window) xe)
  (let ((*tkw* self))
    (unless (find (xevent-type xe) '(:MotionNotify))
      (TRC "main window event" self *tkw* (xevent-type xe)))
    (flet ((give-to-window ()
             (bwhen (eh (event-handler *tkw*))
               (trc "giving to window: eh" eh)
               (funcall eh *tkw* xe))))
      (case (xevent-type xe)
        ((:focusin :focusout) (setf (^focus-state) (xevent-type xe)))
        ((:MotionNotify :buttonpress)
         #+shhh (call-dump-event client-data xe))

        (:configurenotify
         (let ((width (parse-integer (tk-eval "winfo width .")))
               (height (parse-integer (tk-eval "winfo height ."))))
           (trc ":configure-notify >>> widht | height" width height)
           ;; frgo (break "widget-event-handle/:configurenotify")
           #+not (with-cc :configurenotify
             (setf (^width) width)
             (setf (^height) height))))

        (:destroyNotify
         (pushnew *tkw* *windows-destroyed*)
         (ensure-destruction *tkw* :destroyNotify))

        (:virtualevent
         (bwhen (n$ (xsv name xe))
           (trc "main-window-proc :" n$ (unless (null-pointer-p (xsv user-data xe))
                                              (tcl-get-string (xsv user-data xe))))
           (case (read-from-string (string-upcase n$))
             (keypress ;(break "this works??: going after keysym")
               (let ((keysym (tcl-get-string (xsv user-data xe))))
                         (trc nil "keypress keysym!!!!" (tcl-get-string (xsv user-data xe)))
                         (bIf (mod (keysym-to-modifier keysym))
                           (eko (nil "modifiers now")
                             (pushnew mod (keyboard-modifiers *tkw*)))
                           (trc "unhandled pressed keysym" keysym))))
             (keyrelease (break "this works??: going after keysym")
               (let ((keysym (tcl-get-string (xsv user-data xe))))
                           (bIf (mod (keysym-to-modifier keysym))
                             (eko (nil "modifiers now")
                               (setf (keyboard-modifiers *tkw*)
                                 (delete mod (keyboard-modifiers *tkw*))))
                             (trc "unhandled released keysym" keysym))))
             (close-window
              (ensure-destruction *tkw* :close-window))
           
             (window-destroyed
              (ensure-destruction *tkw* :window-destroyed))

             (otherwise
              (give-to-window)))))
        (otherwise (give-to-window)))
      (bwhen (do (post-event-do self))
        (setf (post-event-do self) nil)
        (funcall do self))
      0)))

;; Our own event loop ! - Use this if it is desirable to do something
;; else between events

(defparameter *event-loop-delay*  .10 "Minimum delay [s] in event loop not to lock out IDE (ACL anyway)")

(defparameter *doe-last* 0)

(defun tcl-do-one-event-loop ()
  (app-idle-tasks-clear)
  (loop while (plusp (tk-get-num-main-windows))
      do (loop until (zerop (Tcl_DoOneEvent 2)) ;; 2== TCL_DONT_WAIT
             do (when (and *ctk-dbg* (> (- (now) *doe-last*) 1))
                  (setf *doe-last* (now)))
               (app-idle *app*))
        (app-idle *app*)
        (sleep *event-loop-delay*) ;; give the IDE a few cycles
      finally
        (trc nil "Tcl-do-one-event-loop sees no more windows" *tki*)
        (tcl-delete-interp *tki*) ;; probably unnecessary
        (setf *app* nil *tkw* nil *tki* nil)))

(defmethod window-idle ((self window)))

(defun test-window (root-class &optional (resetp t) &rest window-initargs)
  "nails existing window as a convenience in iterative development"
  (declare (ignorable root-class))

  #+notquite (when (and *tkw* (fm-parent *tkw*)) ;; probably a better way to test if the window is still alive
    (not-to-be (fm-parent *tkw*))
    (setf *tkw* nil ctk::*app* nil))

  (apply 'run-window root-class resetp window-initargs))

